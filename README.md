# AWS Cognito Super Simple Sample 
This is a bare-minimum, static html/js example of how to sign up for, confirm, and authenticate with a AWS Cognito account.

* Create a cognito user pool with the following attributes:
    * email (no alias)
    * name

    _Note the pool id_

* Create an App Client for your user pool
    * don't generate a client secret

    _Note the App client id_

* Update ids.js with the pool id and client id